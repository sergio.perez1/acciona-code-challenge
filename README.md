# Acciona code challenge

El desarrollo de este proyecto ha sido basado en el enunciado del archivo adjunto, donde se solicita la creación de una aplicación basada en Spring Boot que persista tweets que cumplan ciertas condiciones, y, a su vez, tenga una serie de peticiones a gestionar.

## Persistencia
La persistencia en memoria la hemos hecho con h2, y, cada vez que se inicie la aplicación, se resetea.

## Estructura del proyecto

Hay una serie de carpetas con objetivos concretos:
- controller: esta carpeta contiene las peticiones a procesar. Según el enunciado, son necesarias 4 peticiones. 
  
    1. Mostrar los tweets. Mediante la petición "/get_all_tweets".
    2. Obtener los tweets validados para un usuario. La url es: "/get_by_user" y como queryParam se manda username->value.
    3. Actualizar un tweet para validarlo. Es un POST, y la url: "/change_to_validated". Tiene dos queryParam, id->tweet_id, y setTo->true/false para actualizar el valor de ese tweet concreto
    4. Obtener los hashtag más relevantes. Es un GET con la url: "/get_most_relevants", y como queryParam opcional number->total a mostrar.
    
- daos: contiene para la entity Tweet, una interfaz JpaRepository, que permite las operaciones CRUD, y hemos añadido dos queries para la respuesta de las peticiones.
- services: implementa la lógica para simplificar los controller. En el caso de la obtención de los hashtag más utilizados, teníamos que tratarlo de manera especia, ya que un mismo tweet puede tener duplicado varias veces el mismo hashtag, por lo que se hace varios pasos:
    1. Query de TweetRepository para traer todos los tweets que contengan uno o más hashtag
    2. Por cada uno de esos tweets, obtener todos los hashtag y eliminamos duplicados
    3. Nos apoyamos en una estructura auxiliar que implementa el algoritmo de burbuja para ordenación de los más utilizados.
    4. Devolvemos la lista con tantos elementos como se quiera.
    
- twitter: tiene la implementación del streaming y su ingesta en h2.
- utils: recoge una serie de clases que hemos utilizado para simplificar el desarrollo.
    1. GenericFunctions: para testing, aglutina una serie de funciones que son recurrentes, por ejemplo, la comparación de dos listas para ver si son iguales.
    2. KeyItem: Objeto auxiliar que tiene un string (hashtag) y un numérico (apariciones).
    3. LanguageEnum: enumerado con los idiomas permitidos en la ingesta (español, francés e italiano)
    4. OrderedList: es una lista de keyItems, e implementa ordenación de burbuja. Si existe el hashtag lo incrementa y reordena si es necesario, o lo inserta.
  
## Despliegue de la aplicación

Para ejecutar el proyecto, simplemente ejecutamos el comando siguiente.
```bash
mvn clean install spring-boot:run
```
