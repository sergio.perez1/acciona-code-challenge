package com.codechallenge.acciona.controller;

import com.codechallenge.acciona.daos.TweetRepository;
import com.codechallenge.acciona.entities.Tweet;
import com.codechallenge.acciona.services.TweetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TweetController {

    @Autowired
    private TweetService tweetService;

    @GetMapping("/get_all_tweets")
    public List<Tweet> getAllTweets(){
        return tweetService.getAllTweets();
    }

    @PostMapping(value = "/change_to_validated")
    public void validateTweet(@RequestParam (required = true)Long id,  @RequestParam(required = true) Boolean setTo){
        System.out.println("ESTAMOS HERE");
        System.out.println("AQUI "+id+"  Y DE VALOR ---> "+setTo);
        tweetService.setToValidate(id,setTo);
    }

    @GetMapping("/get_by_user")
    public List<Tweet> getTweetsByUser(@RequestParam String username){
        return tweetService.getTweetsValidatedByUser(username);
    }
    //default 10
    @GetMapping("/get_most_relevants")
    public List<String> getMostRelevantHashtag(@RequestParam(defaultValue = "10") Integer number){
        return tweetService.getMostRelevantHashtag(number);
    }

}
