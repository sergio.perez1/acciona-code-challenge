package com.codechallenge.acciona.daos;

import com.codechallenge.acciona.entities.Tweet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface TweetRepository extends JpaRepository<Tweet, Long> {

    @Query(value = "SELECT * FROM TWEETS t WHERE t.VALIDATED IS TRUE AND t.USER= ?1", nativeQuery = true)
    public List<Tweet> getTweetsValidatedByUser(String username);

    @Query(value = "SELECT t.text from TWEETS t where t.text like '%#%'", nativeQuery = true)
    public List<String> getBodyTweetsWithHashtag();
}
