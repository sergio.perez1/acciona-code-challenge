package com.codechallenge.acciona.twitter;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import twitter4j.TwitterException;
import twitter4j.TwitterStream;

@Configuration
public class TwitterConfiguration {
    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
    public TwitterStream initStreaming(Listener listener) throws TwitterException {
       return  listener.initStreaming();
    }
}
