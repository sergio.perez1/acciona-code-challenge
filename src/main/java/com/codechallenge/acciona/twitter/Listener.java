package com.codechallenge.acciona.twitter;

import com.codechallenge.acciona.daos.TweetRepository;
import com.codechallenge.acciona.entities.Tweet;
import com.codechallenge.acciona.utils.LanguageEnum;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RestController;
import twitter4j.*;

@Service
public class Listener {

    private Integer followersToFilter=1500;
    @Autowired
    private TweetRepository tweetRepository;

    public TwitterStream initStreaming() throws TwitterException {
        TwitterStream twitterStream = new TwitterStreamFactory().getInstance().addListener(new StatusListener() {
            @Override
            public void onStatus(Status status) {
                TweetToInsert(status);
            }

            @Override
            public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
                //System.out.println("Got a status deletion notice id:" + statusDeletionNotice.getStatusId());
            }

            @Override
            public void onTrackLimitationNotice(int numberOfLimitedStatuses) {
                //System.out.println("Got track limitation notice:" + numberOfLimitedStatuses);
            }

            @Override
            public void onScrubGeo(long userId, long upToStatusId) {
                //System.out.println("Got scrub_geo event userId:" + userId + " upToStatusId:" + upToStatusId);
            }

            @Override
            public void onStallWarning(StallWarning warning) {
               // System.out.println("Got stall warning:" + warning);
            }

            @Override
            public void onException(Exception ex) {
                ex.printStackTrace();
            }
        }).sample();
        return twitterStream;
    }
    private void TweetToInsert(Status status){
        if(status.getUser().getFollowersCount()>followersToFilter){
            if(languageIsValid(status.getLang())){
                if(status.getText()!=null){
                    Tweet tweet=new Tweet(status.getUser().getName(), status.getText(),status.getUser().getLocation(),status.getLang());
                    System.out.println(tweetRepository.save(tweet));
                }
            }
        }
    }

    private boolean languageIsValid(String language){
        boolean flag=false;
        if(language!=null) {
            LanguageEnum[] array = LanguageEnum.values();
            for (int i = 0; i < array.length && !flag; i++) {
                if(array[i].toString().equals(language)){
                    flag=true;
                }
            }
        }
        return flag;
    }
}