package com.codechallenge.acciona.twitter;

import com.codechallenge.acciona.daos.TweetRepository;
import com.codechallenge.acciona.entities.Tweet;
import com.codechallenge.acciona.utils.LanguageEnum;
import org.springframework.beans.factory.annotation.Autowired;
import twitter4j.Status;

public class TweetToInsert {
    Integer followersToFilter=1500;
    @Autowired
    private TweetRepository tweetRepository;
    public TweetToInsert(Status status){
        if(status.getUser().getFollowersCount()>followersToFilter){
            if(languageIsValid(status.getLang())){
                if(status.getText()!=null){
                    Tweet tweet=new Tweet(status.getUser().getName(), status.getText(),status.getUser().getLocation(),status.getLang());
                    tweetRepository.save(tweet);
                }
            }
        }
    }

    private boolean languageIsValid(String language){
        boolean flag=false;
        if(language!=null) {
            LanguageEnum[] array = LanguageEnum.values();
            for (int i = 0; i < array.length && !flag; i++) {
                if(array[i].toString().equals(language)){
                    flag=true;
                }
            }
        }
        return flag;
    }
}
