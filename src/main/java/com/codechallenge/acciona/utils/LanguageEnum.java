package com.codechallenge.acciona.utils;

public enum LanguageEnum {
    es, fr, it
}