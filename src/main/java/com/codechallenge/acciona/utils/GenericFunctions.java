package com.codechallenge.acciona.utils;

import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

public class GenericFunctions <T>{
    private static GenericFunctions functions;
    public static GenericFunctions getGenericFunctions(){
        if(functions==null){
            functions=new GenericFunctions();
        }
        return functions;
    }
    public void comparingDifferentList(List<T> expected, List<T> generated){
        if(expected.size()== generated.size()){
            for(int i=0;i<expected.size();i++){
                assertEquals(expected.get(i),generated.get(i));
            }
        }
        else{
            fail("Expected size is "+expected.size()+" and generated size is "+generated.size());
        }
    }
}
