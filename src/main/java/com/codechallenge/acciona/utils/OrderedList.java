package com.codechallenge.acciona.utils;

import java.util.ArrayList;
import java.util.List;

public class OrderedList {
    List<KeyItem> orderedList;

    public OrderedList(){
        orderedList=new ArrayList<>();
    }

    private void insertNewElement(String element){
        orderedList.add(new KeyItem(element));
    }

    public void checkAndInsert(String element){
        Integer position=null;
        for(int i=0;i<orderedList.size()&&position==null;i++){
            if(orderedList.get(i).getHashtag().equals(element)){
                position=i;
            }
        }
        if(position==null){
            insertNewElement(element);
        }
        else{
            incrementAndReordenate(position);
        }
    }
    public void incrementAndReordenate(Integer currentPosition){
        boolean needsToChange=true;
        orderedList.get(currentPosition).increment();
        KeyItem current= orderedList.get(currentPosition);
        for(int i=currentPosition-1;i>=0&&needsToChange;i--){
            if(orderedList.get(i).getOccurrences()>=current.getOccurrences()){
                needsToChange=false;//el anterior ya es mayor
            }
            else{
                KeyItem temporal=new KeyItem(orderedList.get(i));//el anterior
                orderedList.get(i).updateTo(orderedList.get(currentPosition));//el anterior ahora es el actual
                orderedList.get(currentPosition).updateTo(temporal);//donde estaba el actual ahora va el de la izquierda
                currentPosition--;
            }
        }

    }
    public List<String> getMostRelevants(Integer numberOfItems){
        List<String> output=new ArrayList<>();
        for(int i=0;i<numberOfItems&&i<orderedList.size();i++){
            output.add(orderedList.get(i).getHashtag());
        }
        return output;
    }
    public KeyItem getItem(Integer position){
        return orderedList.get(position);
    }
    public Integer getSize(){
        return orderedList.size();
    }



}
