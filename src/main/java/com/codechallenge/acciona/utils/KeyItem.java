package com.codechallenge.acciona.utils;

import java.util.Objects;

public class KeyItem {
    private String hashtag;
    private Integer occurrences;

    public KeyItem(String hashtag){
        this.hashtag=hashtag;
        occurrences=1;
    }
    public KeyItem(KeyItem newKeyItem){
        this.hashtag=newKeyItem.hashtag;
        this.occurrences=newKeyItem.occurrences;
    }
    public String getHashtag() {
        return hashtag;
    }

    public void setHashtag(String hashtag) {
        this.hashtag = hashtag;
    }

    public Integer getOccurrences() {
        return occurrences;
    }

    public void increment() {
        this.occurrences++;
    }

    public void updateTo(KeyItem newItem){
        this.occurrences= newItem.occurrences;
        this.hashtag=newItem.hashtag;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        KeyItem keyItem = (KeyItem) o;
        return Objects.equals(hashtag, keyItem.hashtag);
    }

    @Override
    public int hashCode() {
        return Objects.hash(hashtag);
    }

    @Override
    public String toString() {
        return "KeyItem{" +
                "hashtag='" + hashtag + '\'' +
                ", occurrences=" + occurrences +
                '}';
    }
}
