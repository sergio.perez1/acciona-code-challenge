package com.codechallenge.acciona;

import com.codechallenge.acciona.twitter.Listener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import twitter4j.TwitterException;

@SpringBootApplication
public class AccionaApplication {

	public static void main(String[] args) throws TwitterException {
		SpringApplication.run(AccionaApplication.class, args);
	}
}
