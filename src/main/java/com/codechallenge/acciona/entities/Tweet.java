package com.codechallenge.acciona.entities;

import com.codechallenge.acciona.utils.LanguageEnum;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(schema = "public", name = "tweets")
public class Tweet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private String user;
    @Column(length = 500)
    private String text;
    @Column
    private String localization;
    @Column
    private boolean validated;
    @Column(length = 3)
    private String language;

    public Tweet(String user, String text, String localization, String language) {
        this.user = user;
        this.text = text;
        this.localization = localization;
        this.validated =false;
        setLanguage(language);
    }

    public Tweet(Long id, String user, String text, String localization, boolean validated, String language) {
        this.id = id;
        this.user = user;
        this.text = text;
        this.localization = localization;
        this.validated = validated;
        this.language=language;
    }

    public Tweet(){}

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Long getId(){
        return id;
    }
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getLocalization() {
        return localization;
    }

    public void setLocalization(String localization) {
        this.localization = localization;
    }

    public boolean isValidated() {
        return validated;
    }

    public void setValidated(boolean validated) {
        this.validated = validated;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language=language;
    }

    @Override
    public String toString() {
        return "Tweet{" +
                "id=" + id +
                ", user='" + user + '\'' +
                ", text='" + text + '\'' +
                ", localization='" + localization + '\'' +
                ", validated=" + validated +
                ", language='" + language + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tweet tweet = (Tweet) o;
        return validated == tweet.validated && Objects.equals(id, tweet.id) && Objects.equals(user, tweet.user) && Objects.equals(text, tweet.text) && Objects.equals(localization, tweet.localization) && Objects.equals(language, tweet.language);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, user, text, localization, validated, language);
    }
}
