package com.codechallenge.acciona.services;

import com.codechallenge.acciona.daos.TweetRepository;
import com.codechallenge.acciona.entities.Tweet;
import com.codechallenge.acciona.utils.OrderedList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class TweetService {
    @Autowired
    private TweetRepository tweetRepository;

    private OrderedList orderedList;

    public List<Tweet> getTweetsValidatedByUser(String username){
        return tweetRepository.getTweetsValidatedByUser(username);
    }
    public List<Tweet> getAllTweets(){
        return tweetRepository.findAll();
    }
    public List<String> getMostRelevantHashtag(Integer numberOfValues){
        orderedList=new OrderedList();
        List<String> allTweets=tweetRepository.getBodyTweetsWithHashtag();
        formatting(allTweets); //we have all hashtag ordered
        Integer maximum = (numberOfValues==null) ? 10:numberOfValues;
        return orderedList.getMostRelevants(maximum);
    }
    public void setToValidate(Long id, Boolean newStatusValid){
        Optional<Tweet> tweet = tweetRepository.findById(id);
        tweet.get().setValidated(newStatusValid);
        tweetRepository.save(tweet.get());
    }


    private void formatting(List<String> tweetList){
        for(int i=0;i<tweetList.size();i++){
            getAllHashtag(tweetList.get(i));
        }
    }
    private void getAllHashtag(String text){
        List<String> output=new ArrayList<>();
        Pattern MY_PATTERN = Pattern.compile("#(\\w+)");
        Matcher mat = MY_PATTERN.matcher(text);
        Boolean isInList=null;
        while (mat.find()) {
            String find= mat.group(1);
            isInList=false;
            for(int i=0;i<output.size()&&!isInList;i++){
                if(output.get(i).equals(find)){
                    isInList=true;
                }
            }
            if(!isInList) {
                output.add(find);
                orderedList.checkAndInsert(find);
            }
        }
    }
}
