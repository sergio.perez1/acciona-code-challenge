package com.codechallenge.acciona.services;

import com.codechallenge.acciona.daos.TweetRepository;
import com.codechallenge.acciona.utils.GenericFunctions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import static org.junit.jupiter.api.Assertions.*;
import org.mockito.InjectMocks;

import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import static org.mockito.ArgumentMatchers.anyString;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

@DataJpaTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class TweetServiceTest {
    @Mock
    private TweetRepository tweetRepository;
    @InjectMocks
    private TweetService tweetService;

    @BeforeAll
    public void insertData(){
        when(tweetRepository.getBodyTweetsWithHashtag()).thenReturn(getMockTweetsWithHashtag());
        when(tweetRepository.getTweetsValidatedByUser(anyString())).thenReturn(null);
    }
    @Test
    public void testCheckMostRelevantHashtag(){
        List<String> generated =tweetService.getMostRelevantHashtag(null);
        List<String> expected = new ArrayList<>();
            expected.add("Avengers");
            expected.add("SpidermanFilm");
            expected.add("Tarzan");
            expected.add("JusticeLeague");
            expected.add("Hulk");
        GenericFunctions.getGenericFunctions().comparingDifferentList(expected, generated);
    }

    @Test
    public void testCheckIfCallInGetTweetsValidatedByUserWorks(){
        assertEquals(tweetService.getTweetsValidatedByUser("user1"),null);
    }
    private List<String> getMockTweetsWithHashtag(){
        List<String> output=new ArrayList<>();
            output.add("I hate #Tarzan");
            output.add("#SpidermanFilm was amazing");
            output.add("#SpidermanFilm was horrible");
            output.add("#SpidermanFilm is not bad, but I prefer #Avengers");
            output.add("#Avengers is the best film in the world");
            output.add("#Avengers are horrible");
            output.add("#Avengers are best than #JusticeLeague because they have a #Hulk");
        return output;
    }
}
