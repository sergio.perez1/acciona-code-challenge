package com.codechallenge.acciona.daos;

import com.codechallenge.acciona.entities.Tweet;
import com.codechallenge.acciona.utils.GenericFunctions;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.ArrayList;
import java.util.List;

@DataJpaTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class TweetRepositoryTest {
    @Autowired
    private TweetRepository tweetRepository;

    @BeforeAll
    private void insertData(){
        tweetRepository.save(new Tweet(1L,"user1","Ejemplo de texto","Massachusets", false, null));
        tweetRepository.save(new Tweet(2L,"user2","#AquiProbamos","otra localizacion", true, null));
        tweetRepository.save(new Tweet(3L,"user3","#holaMundo","otra localizacion", true, null));
        tweetRepository.save(new Tweet(4L,"user3","#Hula #hoop","otra localizacion", true,null));
    }
    @Test
    public void testGetTweetsByUser(){
       // insertData();
        List<Tweet> generated=tweetRepository.getTweetsValidatedByUser("user3");
        List<Tweet> expected = new ArrayList<>();
            expected.add(new Tweet(3L,"user3","#holaMundo","otra localizacion",true, null));
            expected.add(new Tweet(4L,"user3","#Hula #hoop","otra localizacion",true, null));
        GenericFunctions.getGenericFunctions().comparingDifferentList(expected, generated);
    }
    @Test
    public void testGetHashtag(){
        List<String> generated = tweetRepository.getBodyTweetsWithHashtag();
        List<String> expected = new ArrayList<>();
            expected.add("#AquiProbamos");
            expected.add("#holaMundo");
            expected.add("#Hula #hoop");
        GenericFunctions.getGenericFunctions().comparingDifferentList(expected, generated);
    }
}
