package com.codechallenge.acciona.utils;

import static org.junit.Assert.*;

import org.junit.Test;

import java.util.List;

public class OrderedListTest {

    @Test
    public void testOnlyInsertingDistinctValues(){
        OrderedList orderedList=new OrderedList();
        String key="first";
        orderedList.checkAndInsert(key);
        orderedList.checkAndInsert(key);
        orderedList.checkAndInsert(key);
        Integer occurrencesExpected=3;
        assertTrue(orderedList.getSize().equals(1));//only insert one
        assertEquals(orderedList.getItem(0).getOccurrences(),occurrencesExpected);
    }
    @Test
    public void testOrderingWorksCorrectly(){
        OrderedList orderedList=new OrderedList();
        String [] keys={"first", "second","third"};
            orderedList.checkAndInsert(keys[0]);
            orderedList.checkAndInsert(keys[1]);
            orderedList.checkAndInsert(keys[2]);
            orderedList.checkAndInsert(keys[1]);//change second to position [0]
            orderedList.checkAndInsert(keys[2]);//change third to position [1]
            orderedList.checkAndInsert(keys[2]);//change third to position [0]
            orderedList.checkAndInsert(keys[0]);
        String [] expectedKeys={keys[2], keys[1],keys[0]};
        Integer [] expectedIntegers={3,2,2};
        for(int i=0;i<expectedKeys.length;i++){
            assertEquals(orderedList.getItem(i).getOccurrences(),expectedIntegers[i]);
            assertEquals(orderedList.getItem(i).getHashtag(),expectedKeys[i]);
        }
    }
    @Test
    public void getMostRelevants(){
        OrderedList orderedList=new OrderedList();
        String [] keys={"first", "second","third"};
            orderedList.checkAndInsert(keys[0]);
            orderedList.checkAndInsert(keys[1]);
            orderedList.checkAndInsert(keys[2]);
            orderedList.checkAndInsert(keys[1]);//change second to position [0]
            orderedList.checkAndInsert(keys[2]);//change third to position [1]
            orderedList.checkAndInsert(keys[2]);//change third to position [0]
            orderedList.checkAndInsert(keys[0]);
        List<String> generated=orderedList.getMostRelevants(2);
        String [] expectedKeys={keys[2], keys[1]};
        Integer [] expectedIntegers={3,2};
        for(int i=0;i<expectedKeys.length;i++){
            assertEquals(orderedList.getItem(i).getOccurrences(),expectedIntegers[i]);
            assertEquals(orderedList.getItem(i).getHashtag(),expectedKeys[i]);
        }
    }
}
