package com.codechallenge.acciona.utils;

import org.junit.Test;
import static org.junit.Assert.*;

public class KeyItemTest {

    @Test
    public void testCreatingNewKeyItemWorks(){
        KeyItem toTest=new KeyItem("Item example");
        assertTrue(toTest.getOccurrences().equals(1));
    }

    @Test
    public void testIncrementItemWorks(){
        KeyItem toTest=new KeyItem("Item example");
        Integer numberToIncrement=5;
        for(int i=0;i<numberToIncrement;i++){
            toTest.increment();
        }
        assertTrue(toTest.getOccurrences().equals(numberToIncrement+1));
    }

    @Test
    public void testUpdatingObject() {
        KeyItem objectA = new KeyItem("Item1");
        KeyItem toTest=new KeyItem("Item2");
        toTest.increment();
        objectA.updateTo(toTest);
        assertEquals(objectA, toTest);
    }
}
